﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    public List<GameObject> pooledObjects;
    public GameObject objectToPool;
    public int amountToPool;

    void Awake()
    {
        Debug.Log("Awake(): " + pooledObjects);

    }

    // Start is called before the first frame update
    void Start()
    {
        pooledObjects = new List<GameObject>();
        Debug.Log("Start(): " + pooledObjects);


        for (int i = 0; i < amountToPool; i++)
        {
            GameObject obj = (GameObject)Instantiate(objectToPool);
            obj.SetActive(false);
            obj.name = "shot" + i;
            pooledObjects.Add(obj);
        }
    }

    public GameObject GetPooledObject()
    {
        for (int i = 0; i < pooledObjects.Count; i++)
        {
            if (!pooledObjects[i].activeInHierarchy)
            {

                pooledObjects[i].SetActive(true);
                return pooledObjects[i];
            }

        }
        GameObject shotToReturn = pooledObjects[0];
        pooledObjects = ShiftRight(pooledObjects);
        //3   
        return shotToReturn;
    }

    private static List<T> ShiftRight<T>(List<T> lst)
    {
        List<T> temp = new List<T>();
        for (int i = 1; i < lst.Count; i++)
        {
            temp.Add(lst[i]);
        }
        temp.Add(lst[0]);
        return temp;
    }


}
